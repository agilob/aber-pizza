
package uk.ac.aber.dcs.cs12420.aberpizza.tests;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.*;
import uk.ac.aber.dcs.cs12420.aberpizza.data.Item;
import uk.ac.aber.dcs.cs12420.aberpizza.data.Order;
import uk.ac.aber.dcs.cs12420.aberpizza.data.OrderItem;

/**
 * A test...
 * @author arp12
 */
public class OrderTest {
   
   Order order = new Order();
   OrderItem ordered = new OrderItem();
   
   public OrderTest() {
   
   ordered.setDesription ("Pizza");
   ordered.setPrice (new BigDecimal("2.0"));
   order.addItem (ordered, 3);
   }
   
   
   
   @BeforeClass
   public static void setUpClass() throws Exception {
   }

   @AfterClass
   public static void tearDownClass() throws Exception {
   }

   @Before
   public void setUp() {
   }

   @After
   public void tearDown() {
   }

   /**
    * Test of getOrderedProducts method, of class Order.
    */
   @Test
   public void testGetOrderedProducts() {
      System.out.println("getOrderedProducts");
      Order instance = order;
      List expResult = null;
      Item result = order.getOrderedProducts().get(0); //test if an item was added sucessfully
      assertEquals(ordered, result); //it works if result == null
   }

   /**
    * Test of getNumberOfOrdered method, of class Order.
    */
   @Test
   public void testGetNumberOfOrdered() {
      System.out.println("getNumberOfOrdered");
      Order instance = new Order();
      int expResult = 0;
      int result = instance.getNumberOfOrdered();
      assertEquals(expResult, result);
      // TODO review the generated test code and remove the default call to fail.
   }

   /**
    * Test of setCustomerName method, of class Order.
    */
   @Test
   public void testSetCustomerNameAndGetCustomerName() {
      System.out.println("setCustomerName");
      String name = "arp12";
      Order instance = new Order();
      instance.setCustomerName(name);
      // TODO review the generated test code and remove the default call to fail.
      assertEquals(instance.getCustomerName(), name);
   }

   /**
    * Test of cleanOrder method, of class Order.
    */
   @Test
   public void testCleanOrder() {
      System.out.println("cleanOrder");
      Order instance = new Order();
      instance.cleanOrder();
      //passes if array is clean :)
      assertEquals(new ArrayList<Item>(), instance.getOrderedProducts()); 
   }

   /**
    * Test of addItem method, of class Order.
    */
   @Test
   public void testAddItem() {
      
      /**
       * THIS ONE IS TESTED IN THE CONSTRUCTOR.
       * I think I can skip this test then.
       */
      assertTrue(true);
   }

   /**
    * Test of getSubtotal method, of class Order.
    */
   @Test
   public void testGetSubtotal() {
      
      System.out.println("getSubtotal");
      
      Order instance = order;
      
      ordered.getOrderItemTotal(); //no discount
      
      BigDecimal expResult = new BigDecimal("6.0"); //3 items, 2 pounds each
      BigDecimal result = instance.getSubtotal();
      
      assertEquals(expResult, result);
   }

   /**
    * Test of putSpaces method, of class Order.
    */
   @Test
   public void testPutSpaces() {
      System.out.println("putSpaces");
      int nameLength = 41; //length of the product
      //42 is length of the line in a Recepit window
      int priceLength = 0;
      Order instance = new Order();
      String expResult = " "; //putSpaces should return one <spacebar> string
      String result = instance.putSpaces(nameLength, priceLength);
      assertEquals(expResult, result);
      
   }
   
   /**
    * Test of getCash method, of class Order.
    */
   @Test
   public void testGetCashAndSetCash() {
      
      System.out.println("getCash");
      Order instance = new Order();
      BigDecimal cash = new BigDecimal("5.0");//to setcash
      instance.setCash(cash.toString()); //setting cash in Order object
      
      String expResult = cash.toString();
      BigDecimal result = instance.getCash();
      assertEquals(expResult, result.toString());
   }

   /**
    * Test of getChange method, of class Order.
    */
   @Test
   public void testGetChange() {
      
      System.out.println("getChange");
      Order instance = order;
      
      order.setCash("6.0"); //6 punds
      ordered.getOrderItemTotal(); //no discount
      
      BigDecimal expResult = new BigDecimal("0.0"); // 2 * 3 // 3 is quantity of product, frome above
      BigDecimal result = order.getChange();
      assertEquals(expResult, result);
   }

   /**
    * Test of changeQuantity method, of class Order.
    */
   @Test
   public void testChangeQuantity() {
      System.out.println("changeQuantity");
      int where = 0;
      int quantity = 2;
      order.changeQuantity(where, quantity);
      assertEquals(2, ordered.getQuantity());
   }
   
   
}
