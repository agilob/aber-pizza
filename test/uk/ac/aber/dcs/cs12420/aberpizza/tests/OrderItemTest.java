package uk.ac.aber.dcs.cs12420.aberpizza.tests;

import java.math.BigDecimal;
import org.junit.*;
import static org.junit.Assert.*;
import uk.ac.aber.dcs.cs12420.aberpizza.data.OrderItem;

/**
 *
 * @author arp12
 */
public class OrderItemTest {
   
   public OrderItemTest() {
   }

   @BeforeClass
   public static void setUpClass() throws Exception {
   }

   @AfterClass
   public static void tearDownClass() throws Exception {
   }
   
   @Before
   public void setUp() {
   }
   
   @After
   public void tearDown() {
   }

   /**
    * Test of getQuantity method, of class OrderItem.
    */
   @Test
   public void testGetQuantity() {
      System.out.println("getQuantity");
      
      OrderItem instance = new OrderItem();
      
      int quantity = 2;
      instance.setQuantity(quantity);
      
      int result = instance.getQuantity();
      
      assertEquals(quantity, result);

   }

   /**
    * Test of getOrderItemTotal method, of class OrderItem.
    */
   @Test
   public void testGetOrderItemTotal_one() {
      System.out.println("getOrderItemTotal");
      String discount = "10%";
      
      OrderItem instance = new OrderItem();
      instance.setPrice(new BigDecimal("10.0"));
      instance.setQuantity(3);
      
      BigDecimal expResult = new BigDecimal("27.00"); //10.0 * 3 *90% and two digits after comma
      BigDecimal result = instance.getOrderItemTotal(discount);
      
      assertEquals(expResult, result);
   }
   
   
   /**
    * Test of getOrderItemTotal method, of class OrderItem.
    */
   @Test
   public void testGetOrderItemTotal_two() {
      System.out.println("getOrderItemTotal");//
      String discount = "2,0"; //two pounds discount per item
      OrderItem instance = new OrderItem();
      instance.setPrice(new BigDecimal("10.0"));
      instance.setQuantity(3);
      BigDecimal expResult = new BigDecimal("24.00"); //(10.0 - 2) * 3
      BigDecimal result = instance.getOrderItemTotal(discount);
      assertEquals(expResult, result);
   }

   /**
    * Test of getPrice method, of class OrderItem.
    */
   @Test
   public void testGetPrice() {
      System.out.println("getPrice");
      OrderItem instance = new OrderItem();
      instance.setPrice(new BigDecimal("11.1"));
      BigDecimal expResult = new BigDecimal("11.1");
      BigDecimal result = instance.getPrice();
      assertEquals(expResult, result);
   }

   /**
    * Test of setPrice method, of class OrderItem.
    * Test is made in testGetPrice();
   @Test
   public void testSetPrice() {
      System.out.println("setPrice");
      BigDecimal price = null;
      OrderItem instance = new OrderItem();
      instance.setPrice(price);
      // TODO review the generated test code and remove the default call to fail.
      fail("The test case is a prototype.");
   }
   */
   
   /**
    * Test of getDescription method, of class OrderItem.
    */
   @Test
   public void testGetDescription() {
      System.out.println("getDescription");
      OrderItem instance = new OrderItem();
      instance.setDesription("Large pizza with two beer please.");
      String expResult = "Large pizza with two beer please.";
      String result = instance.getDescription();
      assertEquals(expResult, result);
   }

   
   /**
    * Test of setDesription method, of class OrderItem.
    * Method is tested in testGetDescription();
   @Test
   public void testSetDesription() {
      System.out.println("setDesription");
      String description = "";
      OrderItem instance = new OrderItem();
      instance.setDesription(description);
      // TODO review the generated test code and remove the default call to fail.
      fail("The test case is a prototype.");
   }
   */
   
   
   /**
    * Test of setQuantity method, of class OrderItem.
    * setQuantity is tested in testGetQuantity()
   @Test
   public void testSetQuantity() {
      System.out.println("setQuantity");
      int qa = 0;
      OrderItem instance = new OrderItem();
      instance.setQuantity(qa);
      // TODO review the generated test code and remove the default call to fail.
      fail("The test case is a prototype.");
   }
   */

   
   /**
    * Test of getOrderItemTotal method, of class OrderItem.
    */
   @Test
   public void testGetOrderItemTotal_0args() {
      System.out.println("getOrderItemTotal");
      OrderItem instance = new OrderItem();
      instance.setPrice(new BigDecimal("2.0"));
      instance.setQuantity(2);
      BigDecimal quantity = new BigDecimal("2");
      instance.getOrderItemTotal();
      BigDecimal expResult = new BigDecimal("4.0");;
      BigDecimal result = instance.getTotalPrice();
      assertEquals(expResult, result);
   }
   
}
