package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class is operating on quantity of an item in Gui.
 * @see Gui
 * @author arp12
 */
public class Quantity extends JPanel {

   /**
    * A int which is a quantity of an item..
    */
   private int qua = 0;
   /**
    * Button with method in actionListernet to increase quantity int.
    */
   private JButton plus;
   /**
    * Button with method in actionListernet to decrease quantity int.
    */
   private JButton minus;
   /**
    * A label with informations about quantity.
    */
   private JLabel number;

   /**
    * Makes all the stuff to operate on quantity.
    */
   public Quantity() {

      this.qua = 1;
      this.plus = new JButton("+");
      this.minus = new JButton("-");
      this.number = new JLabel("Quantity: " + qua);
      
      
      plus.setBounds(0, 5, 45, 25);
      
      number.setBounds(60, 5, 100, 25);
      
      minus.setBounds(155, 5, 45, 25);
      
      plus.setVisible(true);
      minus.setVisible(true);
      number.setVisible(true);

      this.setLayout(null);
      this.setBounds(120, 15, 200, 30);
      
      this.add(plus);
      this.add(minus);
      this.add(number);
      this.validate();

      number.validate();

      plus.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent event) {
            if(qua >= 1){
               setQuantity(getQuantity()+1);
               number.repaint();
            }
         }
      });


      minus.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent event) {
            if(qua > 1){
               setQuantity(getQuantity()-1);
               number.repaint();
            }
         }
      });


   } //constructor 

   /**
    * Prints a number of quantity of selected product.
    *
    * @param qua Number to set.
    */
   public void setQuantity(int qua) {
      this.qua=qua;
      if (qua >= 0) {

         this.number.setText("Quantity: " + qua);
         //System.out.println("Quantity: " + qua);
         number.repaint();
      number.revalidate();
      }

      //number.repaint();
      //number.revalidate();
   }
   
   /**
    * Returns a quantity of items.
    * @return Quantity of items
    */
   public int getQuantity(){
      
      return this.qua;
   }

   
   /**
    * Decrease quantity by one.
    *
    * @return number of items to order decreased by one.
    */
   private int decreaceQua() {
      if (qua > 0) {
         return qua = qua--;

      } else {
         return qua;
      }
   }

   
   /**
    * Increase quantity by one.
    *
    * @return number of items to order increased by one.
    */
   private int increaseQua() {
      if (qua > 0) {
         return qua = qua++;
      } else {
         return 0;
      }
   }
}
