package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.awt.Font;
import javax.swing.JLabel;

/**
 * A label in Gui.
 * @author arp12
 */
public class CustomerLabel extends JLabel {

   /**
    * Sets a text label in a GUI.
    */
   public CustomerLabel(){
      
      JLabel jl = new JLabel();
      this.setFont(new Font("Courier New", Font.ITALIC, 12));
      this.setBounds(415, 10, 160, 20);
      this.setVisible(true);
      this.setText("Customer name: ");
      this.setName("customer");
   }
   
   
   
}
