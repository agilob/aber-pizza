
package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import uk.ac.aber.dcs.cs12420.aberpizza.gui.Gui;

/**
 * Button to update a quantity of selected item.
 * @see TillView
 * @author arp12
 */
public class TillQuantity extends JButton{

   /**
    * A reference to a Gui object.
    * @see Gui
    */
   private Gui gui;
   
   /**
    * After changing a quantity of product click update to save it.
    * @param gui Object received to make a reference to Gui.
    */
   public TillQuantity(final Gui gui){
      
      this.gui = gui;
      this.setFont(new Font("Courier New", Font.ITALIC, 12));
      this.setText("Update");
      this.setVisible(true);
      this.setBounds(630, 12, 80, 20);
      this.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent event) {
            gui.changeQuantity();
         }
      });
      
      
   }
}
