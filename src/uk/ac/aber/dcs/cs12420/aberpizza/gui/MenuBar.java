package uk.ac.aber.dcs.cs12420.aberpizza.gui;

import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.xml.stream.XMLStreamException;
import uk.ac.aber.dcs.cs12420.aberpizza.data.Till;

/**
 * In this object we have some objects to make a menu bar at the top of Gui.
 *
 * @see Gui
 * @author arp12
 */
public class MenuBar extends JMenuBar {

   /**
    * A reference to a Gui object.
    */
   private Gui gui;
   /**
    * A reference to a Till object.
    */
   private Till till;

   /**
    * This constructor makes a menuBar in Gui object.
    *
    * @param gui A reference to a Gui.
    * @param till A reference to a Till.
    *
    */
   public MenuBar(final Gui gui, final Till till) {
      this.till = till;
      this.gui = gui;

      this.setBounds(0, 0, 730, 50);
      this.setVisible(true);

      JMenu file = new JMenu("Start");
      JMenu help = new JMenu("Help");

      JMenuItem save = file.add(new JMenuItem(new AbstractAction("Save day") {

         @Override
         public void actionPerformed(ActionEvent ae) {
            try {
               till.save(gui.order.getOrderedProducts(),
                       gui.order.getCustomerName());
            } catch (Exception ex) {
               //nothing?
            }
         }
      }));

      JMenuItem load = file.add(new JMenuItem(new AbstractAction("Load day") {

         @Override
         public void actionPerformed(ActionEvent ae) {
            try {
               till.load();
            } catch (FileNotFoundException ex) {
               //Logger.getLogger(MenuBar.class.getName()).log(Level.SEVERE, null, ex);
            } catch (XMLStreamException ex) {
               //Logger.getLogger(MenuBar.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
      }));



      JMenuItem exit = file.add(new JMenuItem(new AbstractAction("Exit") {

         @Override
         public void actionPerformed(ActionEvent ae) {
            gui.dispose();
         }
      }));


      this.add(file);
      this.add(help);

   }
   
   
}
