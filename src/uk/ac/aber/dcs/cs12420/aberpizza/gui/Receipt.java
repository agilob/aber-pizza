package uk.ac.aber.dcs.cs12420.aberpizza.gui;

import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * This class makes a window with a receipt after paying for an order and with orders after loading data.
 * 
 * @author arp12
 */
public class Receipt extends JFrame {

   /**
    * An object with text area to write a receipt int there.
    */
   private JTextArea jta = new JTextArea();

   /**
    * Describes a size of the window.
    * @param s Size of the windows depends on number of lines to print.
    */
   public Receipt(String s) {

      //System.out.println(s);
      this.setVisible(true);
      this.setTitle("Recipt");

      int count = s.replaceAll("[^\n]", "").length();

      if (count == 0) {
         count = 1;
      }

      this.setSize(320, (25 + count * 18));
      JScrollPane jsb = new JScrollPane(jta);
      jta.setFont(new Font("Monospaced", Font.BOLD, 12));
      jta.setEditable(false);
      jta.setVisible(true);

      this.add(jsb);

   }
   
   /**
    * Sets text in text-area window.
    * @param s A text to set.
    */
   public void setText(String s) {

      jta.setText(s);
   }
}
