package uk.ac.aber.dcs.cs12420.aberpizza.gui;

import java.math.BigDecimal;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import uk.ac.aber.dcs.cs12420.aberpizza.data.Order;

/**
 * A class to see what products are selected to buy.
 *
 * @author arp12
 */
public class TillView extends JTable {

   /**
    * A reference to an Order object.
    */
   
   private Order order;
   /**
    * Names for columns.
    */
   
   Vector<String> columnNames = new Vector<String>();
   /**
    * A vector with data in this table.
    */
   
   Vector<Vector> rowData = new Vector<Vector>();
   /**
    * Model to dynamically operate on a list.
    */
   
   DefaultTableModel model = new DefaultTableModel(rowData, columnNames);
   
   /**
    * Make a table based on the model above.
    */
   JTable table = new JTable(model);

   
   /**
    * Constructor for a list of selected products.
    *
    * @param order
    */
   public TillView(Order order) {


      rowData = new Vector<Vector>();

      columnNames = new Vector<String>();

      columnNames.addElement("Description");
      columnNames.addElement("Quan.");
      columnNames.addElement("Price");

      this.model = new DefaultTableModel(rowData, columnNames);
      this.table = new JTable(model) {

         @Override
         public boolean isCellEditable(int rowIndex, int colIndex) {
            if (colIndex != 1) { //only quantity column is editable
               return false; //dont allow to edit this cell
            }

            return true; //else return true, allow to edit this cell
         }
      };

      this.table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
      this.table.getColumnModel().getColumn(0).setMinWidth(150);
      //this.table.setAutoResizeMode(10);
      this.table.setBounds(0, 0, 275, 260);

      this.setBounds(435, 40, 275, 260);
      this.setLayout(null);

      this.add(table);
   }

   
   /**
    * Clears a list.
    */
   public void clear() {
      rowData.clear();
      this.repaint();
   }

   
   /**
    * Sets a list of products in a list.
    *
    * @param name Name for an Item in a list
    * @param quantity An Integer to change a quantity of Item in Order.
    * @param price Price for an Item
    * to@see Item
    */
   public void setList(String name, int quantity, BigDecimal price) {

      Vector<String> row = new Vector<String>();
      row.add(name);
      row.add(Integer.toString(quantity));
      row.add(price.toString());


      rowData.add(row);
      table.repaint();
      rowData.size();
   } //setList

   
   /**
    * Returns a selected number of row, to change values in Order.
    *
    * @return A int which is number of selected row.
    */
   public int getSelected() {

      return table.getSelectedRow();
   }

   
   /**
    * Returns an updated quantity value.
    *
    * @return Changed quantity value.
    */
   public int getQuantity() {
      int tmp = 1;
      try {
         tmp = Integer.parseInt(table.getValueAt(table.getSelectedRow(), 1).toString());
         //must be parsed to Integer to change quantity in ordersList

      } catch (ArrayIndexOutOfBoundsException ex) {
      } finally { //doesnt work anyway o_O

         return tmp;
      } //finally
   } //get price
   
   
   /**
    * Updates a price for a selected object in a till.
    * @param rowNumber A row number to change a price in it.
    * @param price A price to update/
    */
   public void updatePrice(int rowNumber, BigDecimal price){
       Vector<String> row = new Vector<String>(); //make a new object to replace old one
       row = rowData.get(rowNumber); //clone it.
       row.set(2, price.toString()); //change third column in selected row
       rowData.setElementAt(row, rowNumber);
       table.repaint();
   }
}
