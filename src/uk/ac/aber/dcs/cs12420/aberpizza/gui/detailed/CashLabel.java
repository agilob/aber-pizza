
package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import javax.swing.JLabel;

/**
 * Creates a label with "Cash:" text.
 * @author arp12
 */
public class CashLabel extends JLabel {
   /**
    * Creates a label with "Cash:" string to inform about cash field.
    * @see CashForm
    */
   public CashLabel(){
      
     this.setBounds(570, 310, 110, 20);
     this.setVisible(true);
     this.setText("Cash:");
   }

}
