package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;

/**
 * In a constructor it's set a position of the objects.
 *
 * @see Gui
 * @author arp12
 */
public class Discount extends JTextField {

   /**
    * A filed to type a discount value here.
    */
   public Discount() {
      
      this.setText("Discount in £ or %");
      
      this.setBounds(440, 313, 120, 20);
      this.setVisible(true);
      
      this.addMouseListener(new MouseAdapter(){
         @Override
            public void mouseClicked(MouseEvent e){
                setText();
            }
        });
      
   }

   /**
    * Cleans a field on click.
    */
   private void setText(){
      this.setText("");
   }
   /**
    * Gets value.
    * @return 
    */
   public String getDiscount() {
      return this.getText();
   }
}
