package uk.ac.aber.dcs.cs12420.aberpizza.gui;

import java.math.BigDecimal;
import javax.swing.JFrame;
import javax.swing.JPanel;
import uk.ac.aber.dcs.cs12420.aberpizza.data.Order;
import uk.ac.aber.dcs.cs12420.aberpizza.data.OrderItem;
import uk.ac.aber.dcs.cs12420.aberpizza.data.Till;
import uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed.*;

/**
 * This class makes all the GUI. It connects all the functions of a GUI.
 * Includes some smaller Swing elements like buttons, menu, list of products
 * etc.
 * @inheritDoc
 * @author arp12
 */
public class Gui extends JFrame {

   /**
    * Panel where all Swing elements are placed.
    */
   JPanel panel = new JPanel();
   /**
    * A label with "Customer name: ", it's next to a CustomerName text-box.
    *
    * @see CustomerLabel
    * @see CustomerName
    */
   CustomerLabel customer = new CustomerLabel();
   /**
    * An operator sets customer's name here.
    *
    * @see CustomerName
    */
   CustomerName customerName = new CustomerName();
   /**
    * A label with total price to pay.
    *
    * @see TotalPrice
    */
   TotalPrice totalPrice = new TotalPrice();
   /**
    * A label with "Cash: ". Next to text-box.
    *
    * @see CashLabel
    * @see CashForm
    */
   CashLabel cashLabel = new CashLabel();
   /**
    * Here an operator puts how much did customer gave in cash.
    *
    * @see CashForm
    */
   CashForm cashForm = new CashForm();
   /**
    * Prints a list of products got received from enum, sent by a button.
    *
    * @see ListOfProducts Must see!
    * @see Pizzas
    * @see Drinks
    * @see Sides
    */
   ListOfProducts listOfProducts = new ListOfProducts(this);
   /**
    * A till object to do some thing described in Till.java
    *
    * @see Till
    */
   Till till = new Till();
   /**
    * An Order object.
    *
    * @see Order
    */
   Order order = new Order();
   /**
    * Clean all the things!
    *
    * @see CancelButton
    */
   CancelButton cancelButton = new CancelButton(this);
   
   /**
    * A bar with menu...
    *
    * @see MenuBar
    */
   MenuBar menuBar = new MenuBar(this, this.till);
   /**
    * Clicking on it, makes a receipt for you.
    *
    * @see PayButton
    */
   PayButton payButton = new PayButton(this.order, this.cashForm);
   /**
    * This class has a list of selected products in this order.
    *
    * @see TillView
    */
   TillView tillView = new TillView(this.order);
   /**
    * An object with two buttons and a label with quantity. It is destined to
    * change quantity of a product.
    *
    * @see Quantity
    */
   Quantity quantity = new Quantity();
   /**
    * Allows to change quantity of selected item.
    */
   TillQuantity tillQ = new TillQuantity(this);
   
   /**
    * 
    */
   Discount discount = new Discount();
   
   
   /**
    * A constructor for all this above...
    */
   public Gui() {
      
      super();
      this.setResizable(false); //do _NOT_ allow to resize main window
      getContentPane().add(panel);
      
      panel.setLayout(null); //no layout, everything manually. I prefer this that way
      
      this.validate();
      
      setTitle("AberPizza by @arp12"); //title
      setSize(730, 460); //static size
      
      setDefaultCloseOperation(EXIT_ON_CLOSE);
      
      
      setJMenuBar(menuBar);
      panel.add(tillQ);
      panel.add(listOfProducts);
      panel.add(tillView);
      panel.add(cashForm);
      panel.add(customer);
      panel.add(customerName);
      panel.add(cashLabel);
      panel.add(totalPrice);
      panel.add(payButton);
      panel.add(cancelButton);
      panel.add(quantity);
      panel.add(discount);
   }

   /**
    * Yghm... Sets a string received from a listOfProducts to a TillView, sends
    * a price of product to totalPrice... and so on. I mean everywhere.
    *
    * @param name Name of product to send to tillView.
    * @param price A BigDecimal number - price for a selected product
    * @see ListOfProducts
    * @see TillView
    * @see Order
    */
   public void addProduct(String name, BigDecimal price) {

      name = name.replace("_", " ");

      OrderItem ordered = new OrderItem();

      ordered.setDesription(name);
      ordered.setPrice(price);
      
      int quan = quantity.getQuantity();
      
      if (quan > 0) {
         order.addItem(ordered, quan);
         ordered.getOrderItemTotal(
                 discount.getDiscount());
         
         totalPrice.setPrice(order.getSubtotal());
         
         order.setCustomerName(customerName.getText());
         
         tillView.setList(name, quantity.getQuantity(), ordered.getOrderItemTotal(
                 discount.getDiscount()));
      }
   }
   
   /**
    * Cleans a receipt, order and price.
    */
   public void clean() {

      order.cleanOrder();
      totalPrice.clean();
      tillView.clear();
   }

   /**
    * Sends ints to changeQuantity in Order (which place in order and quantity
    * value).
    */
   public void changeQuantity() {
      //if returned not null, exception
      int row = tillView.getSelected();
      
      order.changeQuantity(row, tillView.getQuantity());
      
      
      tillView.updatePrice(row,
              order.getOrderedProducts().get(row).getOrderItemTotal()); //total price for selected item from tillView 
      totalPrice.setPrice(order.getSubtotal()); //refresh price for whole order

   }
}
