package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import uk.ac.aber.dcs.cs12420.aberpizza.data.Order;
import uk.ac.aber.dcs.cs12420.aberpizza.gui.Receipt;

/**
 * Creates an object with action to call creating a receipt.
 *
 * @see CashForm
 * @see Order
 * @author arp12
 */
public class PayButton extends JButton {

   /**
    * An Order object to make a reference.
    */
   private Order order;
   /**
    * A reference to a CashForm object.
    */
   private CashForm cash;

   /**
    *
    * @param order
    * @param cash
    */
   public PayButton(final Order order, final CashForm cash) {

      this.order = order;
      this.cash = cash;
      this.setText("Pay");
      this.setVisible(true);
      this.setSize(50, 40);
      this.setBounds(445, 365, 110, 20);


      this.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent event) {

            order.setCash(cash.getCash());

            if (order.getRecipt().length() > 1) {

               String s = order.getRecipt().toString();
               
               Receipt recipt = new Receipt(s);
               recipt.setText(s);
            } else {
               System.err.println("Something went totaly wrong!");
            }
         }
      });

   }
}
