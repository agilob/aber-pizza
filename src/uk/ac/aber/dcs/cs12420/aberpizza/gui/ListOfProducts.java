package uk.ac.aber.dcs.cs12420.aberpizza.gui;

import java.awt.Color;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import uk.ac.aber.dcs.cs12420.aberpizza.data.Data;
import uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed.Drinks;
import uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed.Pizzas;
import uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed.Sides;

/**
 * This class makes a list of available products. A list is received from
 * methods from buttons, each of buttons takes values from enum.
 *
 * @author arp12
 * @see Pizzas
 * @see Drinks
 * @see Sides
 * @see Gui
 */
public class ListOfProducts extends JPanel {

   /**
    * Model is necessary to make JList dynamic.
    */
   private DefaultListModel model;
   /**
    * A list with products to select.
    */
   private JList list;
   /**
    * List with products descriptions.
    */
   private List<String> listOfProducts = new ArrayList<String>();
   /**
    * A Gui object to make a reference to a received one in a constructor.
    */
   private Gui gui;

   /**
    * A constructor, builds whole list with buttons. Makes a JPanel in GUI in
    * specified position.
    *
    * @see Gui
    * @param gui This object is necessary to send a selected object
    */
   public ListOfProducts(Gui gui) {

      this.gui = gui;

      final Pizzas pizzas = new Pizzas();
      final Drinks drinks = new Drinks();
      final Sides sides = new Sides();
      
      model = new DefaultListModel();
      list = new JList(model);

      list.addMouseListener(mouseListener);
      
      list.setBounds(20, 10, 230, 210);
      list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // allow to select only one product at once
      list.setSelectedIndex(0); // select first item         
      //this.add(sampleJList);
      
      JScrollPane jscroll = new JScrollPane(list);

      list.setBounds(50, 60, 230, 210);
      
      this.add(list);
      this.add(pizzas);
      this.add(drinks);
      this.add(sides);
      this.setLayout(null);
      this.setVisible(true);
      this.setBounds(20, 50, 400, 300);
      this.setBackground(Color.white); // background is white


      /**
       * Copies a list of drinks to this list.
       */
      drinks.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent event) {
            setList(drinks.getList());
         }
      });
      
      /**
       * Copies a list of pizza to this list.
       */
      pizzas.addActionListener(new ActionListener() {
         
         @Override
         public void actionPerformed(ActionEvent event) {
            setList(pizzas.getList());
         }
      });

      
      /**
       * Copies a list of sides to this list.
       */
      sides.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent event) {
            setList(sides.getList());
         }
      });

   } //constructor
   
  /**
    * ************************************************************************
    */
   
   /**
    * MouseClicked event listening for a object in a list of products. Adds
    * selected object to a GUI, which sends to a OrderItem later.
    */
   MouseListener mouseListener = new MouseAdapter() {

      public void mouseClicked(MouseEvent mouseEvent) {
         String name = null;
         JList theList = (JList) mouseEvent.getSource();
         if (mouseEvent.getClickCount() == 2) { //set to one on touch screen
            int index = theList.locationToIndex(mouseEvent.getPoint());
            if (index >= 0) {

               Object o = theList.getModel().getElementAt(index);

               name = o.toString().replace(" ", "_").toString(); //replace <space> for underline
               name = name.substring(0, name.indexOf("__for")); //cut <underline><underline>for <price>

               gui.addProduct(
                       Data.valueOf(name).toString(), //take name
                       Data.valueOf(name).getPrice()); //take price
            }
         }
      }
   };

   /**
    * Copies a list of special products from the enum to a model in .this.
    */
   private void copyArrayToList() {
      model.clear();
      for (int i = 0; i < listOfProducts.size(); i++) {
         model.addElement(listOfProducts.get(i));
      }

   } // copyArrayToList

   /**
    * Sets the list of products to a received one from buttons.
    *
    * @see Pizzas
    * @see Drinks
    * @see Sides
    * @param lOP A list which is copied to a list of products.
    */
   public void setList(List<String> lOP) { //ListOfProducts

      this.listOfProducts.clear();
      this.listOfProducts = lOP;
      copyArrayToList();
   } //setList
}
