
package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;

/**
 * You put here how much was paid in cash for an order.
 * @author arp12
 */
public class CashForm extends JTextField {
   /**
    * Creates a text field to puts how much did customer pay.
    */
   public CashForm(){
      this.setText("0.0");
      this.setVisible(true);
      this.setBounds(620, 310, 80, 20);
      this.addMouseListener(new MouseAdapter(){
            
         @Override
            public void mouseClicked(MouseEvent e){
                setText();
            }
        });
   }
   /**
    * Returns a value of cash.
    * @return Value how much did customer pay.
    */
   public String getCash(){
      return this.getText();
   }
   
   /**
    * Clears the field.
    */
   private void setText(){
      this.setText("");
   }

}
