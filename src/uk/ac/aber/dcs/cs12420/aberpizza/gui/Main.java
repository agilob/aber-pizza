
package uk.ac.aber.dcs.cs12420.aberpizza.gui;

import javax.swing.SwingUtilities;

/**
 * Run Gui object.
 * @see Gui
 * @author arp12
 */
public class Main {

   /**
    * Main method of whole application.
    * @param arvs A standard parameter.
    * @see Gui
    */
   public static void main(String[] arvs){
      
      SwingUtilities.invokeLater(new Runnable() {
         
         @Override
         public void run() {
            Gui ex = new Gui();
            ex.setVisible(true);
         }
      });
   }
   
}
