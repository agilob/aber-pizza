package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.math.BigDecimal;
import javax.swing.JLabel;
import uk.ac.aber.dcs.cs12420.aberpizza.data.Till;

/**
 * A label information about total price for all item in a list in the Till class.
 * @see Till
 * @author arp12
 */
public final class TotalPrice extends JLabel {
   
   /**
    * Making object with properties.
    */
   public TotalPrice() {
      
      this.setVisible(true);
      this.setBounds(435, 340, 200, 20);
      this.setText("Price:  0.0 pounds");
      
   }
   
   /**
    * Set a price in this label after adding an Item.
    * @param a To override a price object.
    */
   public void setPrice(BigDecimal a) {
      this.setText("Price:  " + a + " pounds");
   }
   
   /**
    * Cleans the label, sets price to 0.0.
    */
   public void clean(){
      this.setText("Price:  0.0 pounds");
   }
}
