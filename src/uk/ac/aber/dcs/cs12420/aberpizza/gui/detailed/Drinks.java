package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import uk.ac.aber.dcs.cs12420.aberpizza.data.Data;

/**
 * A button with method to load a list of drinks from enum.
 * @author arp12
 */
public class Drinks extends JButton {

   /**
    * List of products received from enum with Drink type
    */
   private final List<String> list = new ArrayList<String>();   
   
   /**
    * Constructor for this object in Gui class.
    * Sets static place in GUI.
    */
   public Drinks() {

      this.setText("Drinks");
      this.setBounds(140, 0, 120, 30);
      
   }
   
   /**
    * Sends a list products with Drink type are loaded.
    * @return A list of strings with products descriptions is returned.
    */
   public List<String> getList() {
      for (Data i : Data.values()) {
         if (i.getType().equals("Drink")) { //keep a list fresh after clicking a button
            list.add(i.toString().replace("_", " ") + "  for: " + i.getPrice());
         } //if
      } //for each
      return list;
   }
   
}