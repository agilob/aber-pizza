
package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import uk.ac.aber.dcs.cs12420.aberpizza.gui.Gui;

/**
 * A button to cancel an order.
 * @author arp12
 */
public class CancelButton extends JButton {

   
   /**
    * A reference to Gui.
    */
   private Gui gui;
   
   /**
    * A cancel Button.
    * @param gui A reference to Gui,
    */
   public CancelButton(final Gui gui){
      
      this.gui=gui;
      
      this.setVisible(true);
      this.setText("Clean");
      this.setBounds(600, 365, 110, 20);
      
      this.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent event) {
            System.out.println("cleaning");
            gui.clean();
         }
      });
   }
   
   
   
}
