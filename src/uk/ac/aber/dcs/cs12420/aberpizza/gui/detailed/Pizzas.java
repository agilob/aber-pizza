package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import uk.ac.aber.dcs.cs12420.aberpizza.data.Data;
import uk.ac.aber.dcs.cs12420.aberpizza.gui.ListOfProducts;

/**
 * A button with method to load a list of pizzas from enum.
 * @see Data
 * @see ListOfProducts
 * @author arp12
 */
public class Pizzas extends JButton {
   
   /**
    * A list of product's description from enum.
    */
   private final static List<String> list = new ArrayList<String>();
   
   /**
    * A constructor for a button.
    */
   public Pizzas() {
      
      this.setText("Pizzas");
      this.setBounds(0, 0, 120, 30);

   } // constructor
   
   /**
    * Sends a list of strings to a ListOfProducts to print them in a model.
    * @return A list of strings if returned - products descriptions.
    */
   public List<String> getList() {
      for (Data i : Data.values()) {
         if (i.getType().equals("Pizza")) { //keep a list fresh after clicking a button
            list.add(i.toString().replace("_", " ") + "  for: " + i.getPrice());
         } //if
      } //for each
      return list;
   }
   
}