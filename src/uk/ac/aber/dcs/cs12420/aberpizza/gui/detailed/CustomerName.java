package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;

/**
 * A text box to set customer name in a Gui.
 *
 * @author arp12
 */
public class CustomerName extends JTextField {

   /**
    * In a constructor it's set a position of the objects.
    */
   public CustomerName() {

      JTextField jtf = new JTextField("Anonymous");
      this.setVisible(true);
      this.setBounds(525, 12, 100, 20);
      this.setText("Anonymous");

      this.addMouseListener(new MouseAdapter() {

         @Override
         public void mouseClicked(MouseEvent e) {
            setText();
         }
      });
   }

   /**
    * Clears the field.
    */
   private void setText() {
      this.setText("");
   }
}
