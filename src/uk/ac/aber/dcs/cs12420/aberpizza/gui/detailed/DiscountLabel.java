package uk.ac.aber.dcs.cs12420.aberpizza.gui.detailed;

import java.math.BigDecimal;
import javax.swing.JLabel;

/**
 * In a constructor it's set a position of the objects.
 * @see Gui
 * @author arp12
 */
public class DiscountLabel extends JLabel {

   /**
    * A price to pint in this label.
    */
   BigDecimal discount = new BigDecimal(0.0);

   /**
    * Constructor sets a text in this.label.
    */
   public DiscountLabel() {

      this.setVisible(true);
      this.setBounds(445, 310, 110, 20);
      this.setText("Discount:  " + discount.toString());

   }

   /**
    * Changes A text in this.label.
    * @param s a BigDecimal number to set in this.label.
    */
   public void setDiscount(BigDecimal s) {
      discount = s;
   }
}
