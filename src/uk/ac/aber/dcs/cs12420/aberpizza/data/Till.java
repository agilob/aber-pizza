package uk.ac.aber.dcs.cs12420.aberpizza.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JFileChooser;
import javax.xml.stream.*;
import uk.ac.aber.dcs.cs12420.aberpizza.gui.Receipt;
import uk.ac.aber.dcs.cs12420.aberpizza.gui.TillView;

/**
 * This class is responsible for saving and loading tills to a XML file. And
 * exchanging data with TillView class.
 *
 * @see TillView
 * @author arp12
 */
public class Till {

   /**
    * List of loaded items in the list to save in XML file.
    */
   List<String> loaded = new ArrayList<String>();
   
   
   /**
    * Uses XMLStream to save a till.
    *
    * @param list List of items to save in a file.
    * @param customer Customer name to save a bill for they.
    * @throws Exception Exception from XMLStream.
    */
   public void save(List<Item> list, String customer) throws Exception {

      Format ddmmyy,
              hhmmss;

      Date date = new Date();

      ddmmyy = new SimpleDateFormat("dd-MM-yy");
      hhmmss = new SimpleDateFormat("HH-mm-ss");


      String dateS = ddmmyy.format(date);
      String timeS = hhmmss.format(date);

      String path = "../../" + dateS + "/";
      boolean mkdir = new File(path).mkdir();


      String fileName = (path + timeS + ".xml"); //file name and path
      //System.out.println(fileName);

      XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
      XMLStreamWriter writer = null;
      writer = outputFactory.createXMLStreamWriter(new FileOutputStream(fileName), "UTF-8");

      //writer.writeStartDocument(); //XML Standard //OK. this should be here
      // but when it is here, reading doesnt work popery

      writer.writeStartElement("Till"); //start Till tag
      
      writer.writeStartElement("Time"); // start day tag

      ddmmyy = new SimpleDateFormat("HH:mm:ss");
      writer.writeCharacters(ddmmyy.format(date).toString());
      writer.writeEndElement(); // stop day tag

      writer.writeStartElement("Customer"); //start customer tag
      writer.writeCharacters(customer);
      writer.writeEndElement(); //end customer tag

      writer.writeStartElement("Products"); //start product list


      for (int i = 0; i < list.size(); i++) {

         writer.writeStartElement("Item"); //for each item in the list

         writer.writeStartElement("Name"); //item description tag
         writer.writeCharacters(list.get(i).getDescription());
         writer.writeEndElement(); //end of item description

         writer.writeStartElement("Price"); //starts a price tag
         writer.writeCharacters(list.get(i).getTotalPrice().toString());
         writer.writeEndElement(); //end of price tag

         writer.writeEndElement(); // item
      }

      writer.writeEndElement(); //end products tag

      writer.writeEndElement();//end till tag
      writer.writeEndDocument(); //end xml tag

      writer.flush(); //flush
      writer.close(); //close...

   }

   /**
    * Loads a till from a specified date.
    *
    * @return Returns a string to print in a Receipt window.
    * @see Receipt
    * @throws FileNotFoundException If file is not found...
    * @throws XMLStreamException If XML is not valid.
    */
   public Till load() throws FileNotFoundException, XMLStreamException {
      XMLInputFactory inputFactory = XMLInputFactory.newInstance();

      Format ddmmyy;

      Date date = new Date();

      ddmmyy = new SimpleDateFormat("dd-MM-yy");

      String dateS = ddmmyy.format(date);
      String path = "";


      JFileChooser chooser = new JFileChooser(); //to choose a file
      chooser.setCurrentDirectory(new java.io.File("../")); //lower directory
      chooser.setDialogTitle("Select day to load");
      chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); //choose a directory
      chooser.setAcceptAllFileFilterUsed(false);

      if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
         path = chooser.getSelectedFile().toString() + "/";
      } else {
         System.err.println("Nothing selected."); //notify about error
         path = "../../" + dateS + "/"; //default directory for today
      }

      File actual = new File(path);

      String recipt = "";

      for (File f : actual.listFiles()) { //for each file in a directory

         //System.out.println(path + f.getName());

         recipt = recipt + "/***********************/\n\n";
         recipt = recipt + "       AberPizza\n";
         recipt = recipt + "Ordered: ";
         recipt = recipt + dateS + " at ";

         int control = 0;
         int total = 0;
         XMLStreamReader reader = inputFactory.createXMLStreamReader(
                 new FileInputStream(path + f.getName().toString()));
         while (reader.hasNext()) {

            int event = reader.next();


            if (event == XMLStreamConstants.CHARACTERS) {

               if (control == 0) {
                  recipt = recipt + reader.getText().toString().replaceAll("\n", "").replaceAll("\n", "") + "\n";
               } else if (control == 1) {
                  recipt = recipt + "\nFor customer:  " + reader.getText().toString().replaceAll("\n", "") + "\n";
               } else if (control % 2 == 0) {
                  recipt = recipt + reader.getText().toString().replaceAll("\n", "");
                  recipt = recipt + "\t";
               } else if (control % 2 == 1) {
                  recipt = recipt + reader.getText().toString() + "\n";
               }

               control++;
            }
         }
      }
      recipt = recipt + "\n\n\n";
      Receipt recipt1 = new Receipt(recipt);
      recipt1.setText(recipt);
      return this;
   }
}