
package uk.ac.aber.dcs.cs12420.aberpizza.data;

import java.math.BigDecimal;

/**
 * Design of this class is from specification. With small modifications. I added two methods declarations.
 * @author arp12
 */
public interface Item {

   public BigDecimal getTotalPrice();
   
   public BigDecimal getPrice();
   
   public void setPrice(BigDecimal price);
   
   public String getDescription();
   
   public void setDesription(String description);

   public BigDecimal getOrderItemTotal();

   public void setQuantity(int quantity);
      
}
