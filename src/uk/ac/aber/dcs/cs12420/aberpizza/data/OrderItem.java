package uk.ac.aber.dcs.cs12420.aberpizza.data;

import java.math.BigDecimal;

/**
 * An item to set in Order class in a list of items.
 *
 * @author arp12
 */
public class OrderItem implements Item {

   /**
    * Quantity of item.
    */
   private int quantity;
   /**
    * Price of a item.
    */
   private BigDecimal price;
   /**
    * Description of a item.
    */
   private String desctiption;
   
   /**
    * Save a value of discount.
    */
   BigDecimal totalPrice;
   
   
   
   /**
    * Returns a quantity of an item.
    *
    * @return quantity of an item.
    */
   public int getQuantity() {

      return quantity;
   }

   /**
    * Computes price for every Item in an order.
    *
    * @param discountValue A string which is value of discount in % or pound per ONE product
    * @return BigDecimal object which is a total price for an order.
    */
   public BigDecimal getOrderItemTotal(String discountValue) {

      BigDecimal value = getPrice();
      
      if (quantity >= 3) {
         if (discountValue.matches("[0-9]{1,2}%")) {
            discountValue = discountValue.substring(0, discountValue.length()-1); //remove '%' char
            BigDecimal hundred = new BigDecimal(100); //to get percentage value of price
            BigDecimal disc = new  BigDecimal(discountValue); //discount in BigDecimal value
            BigDecimal percentage =
                    (hundred.subtract(disc)).divide(hundred); //(100 - n%)/100 = 0.n discount
            value = price.multiply(percentage).multiply(new BigDecimal(quantity)); // (price * 0.n) == (n% * price)
            
         } //if
         else if(discountValue.matches("[0-9].[0-9]") || //if discount is in different formats
                 discountValue.matches("[0-9]") ||
                 discountValue.matches("[0-9],[0-9]")){
            
            if(discountValue.contains(",") == true){ // without this tests is failed
               discountValue = discountValue.replaceAll(",", ".");
            } // a float number with comma is not a proper BigDecimal value to use in constructor
            
            value = value.subtract(new BigDecimal(discountValue)); //substract some pound from item price
            value = value.multiply(new BigDecimal(quantity)); //multipy by number of items
         } //else if
         
         else{
            System.err.println("Wrong format od discount!"); //quiet notification about wrong format
            // ssshhhhhhhhhh.....
            value = value.multiply(new BigDecimal(quantity)); //set normaln price
         } //else
      }
      else{
         value = value.multiply(new BigDecimal(quantity));
      }
      
      setTotalPrice(value.setScale(2, BigDecimal.ROUND_HALF_DOWN));
      return getTotalPrice();
   }
   
   /**
    * Sets a discount value.
    * @return 
    */
   private void setTotalPrice(BigDecimal a){
      this.totalPrice = a;
   }
   
   /**
    * Gets a value of discount.
    * @return 
    */
   public BigDecimal getTotalPrice(){
      return this.totalPrice;
   }

   @Override
   public BigDecimal getPrice() {

      return price;
   }

   @Override
   public void setPrice(BigDecimal price) {

      this.price = price;
   }

   @Override
   public String getDescription() {

      return desctiption;
   }

   @Override
   public void setDesription(String description) {

      this.desctiption = description;
   }

   /**
    * Sets quantity of an item.
    *
    * @param qa
    */
   @Override
   public void setQuantity(int qa) {

      this.quantity = qa;
   }

   @Override
   public BigDecimal getOrderItemTotal() {
      setPrice(price.multiply(new BigDecimal(quantity)));
      return getPrice();
   }


}
