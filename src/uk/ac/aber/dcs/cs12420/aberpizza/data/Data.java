package uk.ac.aber.dcs.cs12420.aberpizza.data;

import java.math.BigDecimal;

/**
 * Enumerated data structure with different kinds of products to buy.
 * Very easy to extend.
 * @author arp12
 */
public enum Data {

   
   /**
    * Pizza for 5.0
    */
   Oregano_small(new BigDecimal("5.0"), "Pizza"),
   /**
    * Pizza for 7.0
    */
   Oregano_medium(new BigDecimal("7.0"), "Pizza"),
   /**
    * Pizza for 10.0
    */
   Oregano_large(new BigDecimal("10.0"), "Pizza"),
   /**
    * Pizza for 5.5
    */
   Chicken_small(new BigDecimal("5.5"), "Pizza"),
   /**
    * Pizza for 7.5
    */
   Chicken_medium(new BigDecimal("7.5"), "Pizza"),
   /**
    * Pizza for 10.0
    */
   Chicken_large(new BigDecimal("11.0"), "Pizza"),
   /**
    * Pizza for 6.0
    */
   Meat_small(new BigDecimal("6.0"), "Pizza"),
   /**
    * Pizza for 8.0
    */
   Meat_medium(new BigDecimal("8.0"), "Pizza"),
   /**
    * Pizza for 12.0
    */
   Meat_large(new BigDecimal("12.0"), "Pizza"),
   
   
   /**
    * Drink for 2.0
    */
   Coke(new BigDecimal("2.0"), "Drink"),
   /**
    * Drink for 2.0
    */
   Orange_juice(new BigDecimal("2.0"), "Drink"),
   /**
    * Drink for 2.1
    */
   Lemonade(new BigDecimal("2.1"), "Drink"),
   
   
   /**
    * Side for 3.1
    */
   Gralic_bread(new BigDecimal("3.1"), "Side"),
   /**
    * Side for 4.0
    */
   Fries(new BigDecimal("4.0"), "Side"),
   /**
    * Side for 3.5
    */
   Potato_wedges(new BigDecimal("3.5"), "Side"),
   /**
    * Side for 4.2
    */
   Coleslaw(new BigDecimal("4.2"), "Side");
   
   
   /**
    * Price for a product.
    */
   BigDecimal price;
   
   
   /**
    * Name of a product.
    */
   String type;

   
   /**
    * Constructor for a product.
    * @param a Changed to price of a product.
    * @param b Changed to type of a product.
    * @author arp12
    */
   Data(BigDecimal a, String b) {
      price = a;
      type = b;
   }

   
   /**
    * Returns a BigDecimal number which is price for a product.
    * @return Price of selected product from enum.
    */
   public BigDecimal getPrice() {
      return price;
   }

   
   /**
    * Returns a type of a product.
    * @return Type of a product, pizza, drinks or sides.
    */
   public String getType() {

      return type;
   }
}
