package uk.ac.aber.dcs.cs12420.aberpizza.data;

import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class has data to operate on whole application.
 * @author arp12
 */
public class Order extends OrderItem implements Item {

   /**
    * A Date to a set it in a Receipt.
    */
   private Date date;
   
   /**
    * A customer name to set in in a Receipt.
    */
   private String customerName = "";
   /**
    *  A cash paid to set in in a Receipt.
    */
   private BigDecimal cash;
   
   /**
    * List of products to set in in a Receipt.
    */
   private List<Item> ordersList = new ArrayList<Item>();
   /**
    * A price for a object to set in in a Receipt.
    */
   private BigDecimal price = new BigDecimal("0.0");

   /**
    * Simple constructor.
    */
   public Order() {
   }

   
   /**
    * This list contains a list of products selected to buy.
    * @return A list of products to print it in the TillView and XMLSerialize them etc.
    */
   public List<Item> getOrderedProducts() {
      
      return this.ordersList;
   }

   /**
    * Returns a number of ordered items.
    * @return A number of ordered products.
    */
   public int getNumberOfOrdered(){
      
      return ordersList.size();
   }
   
   /**
    * Sets a customer name in this order.
    * @param name 
    */
   public void setCustomerName(String name) {

      customerName = name;
   }

   
   /**
    * Returns a customer name.
    * @return A customer name.
    */
   public String getCustomerName() {

      return customerName;
   }

   
   /**
    * Cleans the list with orders.
    */
   public void cleanOrder() {

      ordersList.clear();
   }

   
   /**
    * Adds an item to the list with specified number of quantities.
    * @param item Is an object added to a list of ordered products
    * @param quantity How many times the product was ordered.
    */
   public void addItem(Item item, int quantity) {
      
         item.setQuantity(quantity);
         ordersList.add(item);
      
   }

   
   /**
    * Returns a BigDecimal number - how much are the products on the list.
    * @return How much are products on the list.
    */
   public BigDecimal getSubtotal() {
      
      price = new BigDecimal("0.0");
      
      for (int i = 0; i < ordersList.size(); i++) {
         price = price.add(ordersList.get(i).getTotalPrice());
      }
      
      return price;
   }

   /**
    * Returns a discount value.
    * @return Discount value.
    */
   public BigDecimal getDiscount() {
      
      return new BigDecimal(5.0);
   }

   /**
    * Formats, makes whole receipt format, makes all what is on a receipt.
    * @return A string which is printed in a Receipt class.-Xlint:unchecked
    */
   public String getRecipt() {

      String receipt = "";
      Format formatter;

      Date date = new Date();
      formatter = new SimpleDateFormat("HH:mm:ss dd-MM-yy"); //format for a receipt

      String dateS = formatter.format(date);
      receipt = receipt + "       AberPizza\n";
      receipt = receipt.concat(putSpaces(-1, dateS.length() + 9) //make a proper format of a line
              + "Ordered: "
              + dateS); //date of the order

      receipt = receipt.concat("\n\nFor customer:" 
              + putSpaces(13, // "For customer:".length()+3; format the line
              getCustomerName().length()) 
              + getCustomerName()
              + "\n");

      for (int i = 0; i < ordersList.size(); i++) {

         receipt = receipt.concat(" " + ordersList.get(i).getDescription().toString());
         receipt = receipt.concat(
                 putSpaces(ordersList.get(i).getDescription().length(),
                 ordersList.get(i).getTotalPrice().toString().length())
                 + ordersList.get(i).getTotalPrice().toString() + "\n\n");

      }

      receipt = receipt.concat("\n\n|------------------------------------------|\n");



      receipt = receipt.concat("\n\nTo pay: "
              + putSpaces(7, //"To pay:".length()
              getSubtotal().toString().length())
              + getSubtotal().toString());
      if (getCash() != null) {
         receipt = receipt.concat("\n\nCASH:"
                 + putSpaces(4, //"CASH:".length()
                 getCash().toString().length())
                 + getCash());
      }

      receipt = receipt.concat("\n\nChange:"
              + putSpaces(6,//"Change:".length()
              getChange().toString().length())
              + getChange());

      return receipt;

   }

   
   /**
    * This method is making proper format of line in a receipt.
    *
    * @param nameLength Length of the first string
    * @param priceLength Length of the second string
    * @return String fully made from spaces
    */
   public String putSpaces(int nameLength, int priceLength) {

      String spaces = "";

      int totalLength = 42;

      totalLength = totalLength - nameLength - priceLength;

      for (int i = 0; i < totalLength; i++) {
         spaces += " ";
      }
      return spaces;
   }

   /**
    * Sets a value of cash.
    * @param cash To set this.cash from received object.
    */
   public void setCash(String cash) {

      //casting from string to BigDecimal, works fine, dont touch
      this.cash = new BigDecimal(cash);
   }
   
   /**
    * Returns a value of paid cash.
    * @return Cash...
    */
   public BigDecimal getCash() {

      return this.cash;
   }

   /**
    * Returns a change for an order.
    * @return A value of change.
    */
   public BigDecimal getChange() {
      
      BigDecimal cha = getCash().subtract(getSubtotal()).abs();
      
      return cha;
   }
   
   /**
    * Changing quantity of selected item.
    * @param where Number of row where to change quantity
    * @param quantity A price to replace previous.
    */
   public void changeQuantity(int where, int quantity){
      if(quantity > 0){
         Item iitem = ordersList.get(where);
         iitem.setQuantity(quantity);
         ordersList.set(where, iitem);
      }
   }
   
}
